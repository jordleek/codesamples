﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ingredient", menuName = "ScriptableObjects/Ingredient")]
public class Ingredient : ScriptableObject, IComparable<Ingredient>
{
    public IngredientName Name = IngredientName.Noodles;
    public Sprite Sprite;
    public AcceptableActions AcceptFlags;
    public int AmountNeededToCut = 3;
    public int CompareTo(Ingredient other)
    {
        if ((int)Name > (int)other.Name)
        {
            return 1;
        }
        else if ((int)Name < (int)other.Name)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }

    /// <summary>
    /// Returns a boolean depending on if the ingredient is cut.
    /// </summary>
    public bool IsCut => Name.CheckFlag(IngredientName.CutIndex);

    /// <summary>
    /// Returns a boolean depending on if the ingredient is allowed to be cut.
    /// </summary>
    public bool AllowedToCut => AcceptFlags.CheckFlag(AcceptableActions.Cuttable) && !IsCut;

    /// <summary>
    /// Returns a boolean depending on if the ingredient is allowed in a pot.
    /// </summary>
    public bool AllowedInPot => AcceptFlags.CheckFlag(AcceptableActions.PotAllowed);

    /// <summary>
    /// Returns a boolean depending on if the ingredient is allowed in a pan.
    /// </summary>
    public bool AllowedInPan => AcceptFlags.CheckFlag(AcceptableActions.PanAllowed);

}
[Serializable]
[Flags]
public enum IngredientName : short
{
    Bun         = 1,
    Cabbage     = 2,
    Cheese      = 4,
    Fish        = 8,
    Meat        = 16,
    Noodles     = 32,
    Tomato      = 64,
    CutIndex    = 128
}

[Flags]
public enum AcceptableActions : byte
{
    Cuttable    = 1,
    PotAllowed  = 2,
    PanAllowed  = 4
}