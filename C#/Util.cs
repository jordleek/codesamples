﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

internal static class Util
{
    /// <summary>
    /// Get the closest GameObject form a list relative to a position.
    /// </summary>
    /// <param name="position">The position you want to know the closest object to.</param>
    /// <param name="objects">A list of GameObject that you want to check.</param>
    /// <returns>A tuple containing the distance to the object as a float and the GameObject that is closest</returns>
    public static (float Distance, GameObject Object) GetClosestObject(this Vector3 position, [NotNull] ref List<GameObject> objects)
    {
        if(objects.Count <= 0) return (-1, null);

        GameObject closest = null;
        var dist = Mathf.Infinity;

        foreach (var obj in objects)
        {
            var newDist = Vector3.Distance(position, obj.transform.position);
            if (!(newDist < dist)) continue;

            dist = newDist;
            closest = obj;
        }

        return (dist, closest);
    }

    /// <summary>
    /// Get the closest GameObject form a list relative to a Transform.
    /// </summary>
    /// <param name="transform">The Transform you want to know the closest object to.</param>
    /// <param name="objects">A list of GameObject that you want to check.</param>
    /// <returns></returns>
    public static (float Distance, GameObject Object) GetClosestObject([NotNull] this Transform transform, [NotNull] ref List<GameObject> objects)
    {
	    return transform.position.GetClosestObject(ref objects);
    }

    /// <summary>
    /// Get the closest GameObject form a list relative to a GameObject.
    /// </summary>
    /// <param name="gameObject">The GameObject you want to know the closest object to.</param>
    /// <param name="objects">A list of GameObject that you want to check.</param>
    /// <returns></returns>
    public static (float Distance, GameObject Object) GetClosestObject([NotNull] this GameObject gameObject, [NotNull] ref List<GameObject> objects)
    {
	    return gameObject.transform.GetClosestObject(ref objects);
    }

    /// <summary>
    /// Get the closest GameObject form a list relative to a position.
    /// </summary>
    /// <param name="objects">A list of GameObject that you want to check.</param>
    /// <param name="position">The position you want to know the closest object to.</param>
    /// <returns></returns>
    public static (float Distance, GameObject Object) GetClosestObject([NotNull] this List<GameObject> objects, Vector3 position)
    {
	    return position.GetClosestObject(ref objects);
    }
    /// <summary>
    /// Get the closest GameObject form a list relative to a Transform.
    /// </summary>
    /// <param name="objects">A list of GameObject that you want to check.</param>
    /// <param name="transform">The Transform you want to know the closest object to.</param>
    /// <returns></returns>
    public static (float Distance, GameObject Object) GetClosestObject([NotNull] this List<GameObject> objects, [NotNull] Transform transform)
    {
	    return transform.GetClosestObject(ref objects);
    }
    /// <summary>
    /// Get the closest GameObject form a list relative to a GameObject.
    /// </summary>
    /// <param name="objects">A list of GameObject that you want to check.</param>
    /// <param name="gameObject">The GameObject you want to know the closest object to.</param>
    /// <returns></returns>
    public static (float Distance, GameObject Object) GetClosestObject([NotNull] this List<GameObject> objects, [NotNull] GameObject gameObject)
    {
	    return gameObject.GetClosestObject(ref objects);
    }
}

internal static class EnumUtil
{
    /// <summary>
    /// This method check if a flag is set in an Enum field with the [Flag] attribute.
    /// </summary>
    /// <typeparam name="T">This should be a Enum with the [Flag] attribute.</typeparam>
    /// <param name="flags">The Enum field that has to be checked.</param>
    /// <param name="flagToCheck">The flags which should be in the Enum field.</param>
    /// <returns>A boolean that is true if the <paramref name="flags"/> contains the <paramref name="flagToCheck"/></returns>
    public static bool CheckFlag<T>(this T flags, T flagToCheck) where T : Enum
    {
        return Convert.ToBoolean(Convert.ToInt16(flags) & Convert.ToInt16(flagToCheck));
    }
}
