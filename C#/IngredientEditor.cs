﻿using System;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(Ingredient))]
public class InteractionManagerEditor : Editor
{
    public Ingredient Ingredient => target as Ingredient;

    /// <summary>
    /// This method handles what is drawn when the inspector of the specified object above is open.
    /// </summary>
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();
        //This handles the name grid.
        GUILayout.BeginHorizontal();
        GUILayout.Label("Name");

        //Setting up the values to call DisplayGrid.
        var ingredientName = serializedObject.FindProperty("Name");
        var enumNames = ingredientName.enumNames;
        var amount = enumNames.Length - 1;
        var nameOfSelected = (Ingredient.Name ^ (Ingredient.IsCut ? IngredientName.CutIndex : 0)).ToString();

        //Getting the new values for the ingredient name.
        var values = DisplayGrid(enumNames, amount, nameOfSelected);

        //Set the new values.
        for (var i = 0; i < values.Length; i++)
        {
            if (!values[i]) continue;
            ingredientName.intValue = (int)((IngredientName) (1 << i) | (Ingredient.IsCut ? IngredientName.CutIndex : 0));
        }
        GUILayout.EndHorizontal();

        //This handles the acceptFlags grid 
        GUILayout.BeginHorizontal();
        GUILayout.Label("AcceptFlags");

        //Setting up the values to call DisplayGrid.[Important] reuse of vars
        var acceptFlags = serializedObject.FindProperty("AcceptFlags");
        enumNames = acceptFlags.enumNames;
        amount = enumNames.Length;
        nameOfSelected = Ingredient.AcceptFlags.ToString();

        //Getting the new values for the ingredient acceptFlags.[Important] reuse of vars
        values = DisplayGrid(enumNames, amount, nameOfSelected, true);

        //Set the new values.
        var temp = 0;
        for (var i = 0; i < values.Length; i++)
        {
            if (!values[i]) continue;
            temp |= (1 << i);
        }
        acceptFlags.intValue = temp;
        GUILayout.EndHorizontal();

        //Displaying if the ingredient is cut or not.
        GUILayout.BeginHorizontal();
        GUILayout.Label("Is Cut");

        var isCut = Ingredient.IsCut;
        isCut = GUILayout.Toggle(isCut, "");

        if (isCut != Ingredient.IsCut)
        {
            ingredientName.intValue = (int)(Ingredient.Name ^ IngredientName.CutIndex);
        }
        GUILayout.EndHorizontal();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("AmountNeededToCut"), false);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Sprite"), false);
        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
        }
    }

    /// <summary>
    /// Handles the drawing of the grid that displays what the value of the enum is.
    /// </summary>
    /// <param name="enumNames"> An array of all the names in the Enum.</param>
    /// <param name="amount"> The count of how many names of the Enum you want to display.</param>
    /// <param name="selectedName">The currently selected values of the Enum.</param>
    /// <param name="allowMultiple">A boolean that handles if you want multiple values back or only allow 1.</param>
    /// <returns>A boolean array of the size specified by <paramref name="amount"/>.</returns>
    private static bool[] DisplayGrid(string[] enumNames, int amount, string selectedName, bool allowMultiple = false) 
    {
        var values = new bool[amount];
        var names = selectedName.Replace('(', ' ').Replace(')', ' ').Trim().Split(',');
        
        foreach (var curName in names)
        {
            if (curName.Trim() == "0") continue;
            var exists = Array.Exists(enumNames, x => x == curName.Trim());
            if (exists)
            {
                values[Array.IndexOf(enumNames, curName.Trim())] = true;
                continue;
            }
            Debug.LogError($"The name: {curName.Trim()} could not be found inside of this collection: {enumNames}");
            return values;
        }

        GUILayout.BeginHorizontal();
        for (var col = 0; col < 3; col++)
        {
            GUILayout.BeginVertical();
            for (var i = col; i < amount; i += 3)
            {
                if (allowMultiple)
                {
                    values[i] = GUILayout.Toggle(values[i], $"{enumNames[i]}");
                }
                else
                {
                    var value = GUILayout.Toggle(values[i], $"{enumNames[i]}");
                    if (!value)
                    {
                        continue;
                    }
                    values[Array.IndexOf(enumNames, selectedName)] = false;
                    values[i] = true;
                }
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();
        return values;
    }
} 